<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Post;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $viewPage = 'admin.post.';

    public function index()
    {
        $posts = Post::all();
//        dd($posts);
        return view($this->viewPage.'index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view($this->viewPage.'create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required',
        ],
            [
                'category_id.required' => 'Category Name must not be empty!',
                'title.required' => 'Post Title must not be empty!',
            ]);

        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        //$image = $request->file('image');

        //$name = 'orgd150718.png';
        if($request->file('image')!=null && $request->file('image')->isValid()) {
            try {
                $file = $request->file('image');
                $tempName = strtolower(str_replace(' ', '', $request->input('title')));
                $name = $tempName.date("dmy").'.' . $file->getClientOriginalExtension();

                $request->file('image')->move("images/post", $name);
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
//                    return false;
            }
        }
        $postImage = '';
        if(!empty($name)){
            $postImage = "images/post/".$name;
        }

        $post = new Post();

        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->image = $postImage;
        $post->description = $request->description;
        $post->status = $request->status;

        $post->save();

        return redirect()->back()->with('success','Post created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
//        dd($post);
        return view($this->viewPage.'show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $post = Post::findOrFail($id);
        return view($this->viewPage.'edit',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required',
        ],
            [
                'category_id.required' => 'Category Name must not be empty!',
                'title.required' => 'Post Title must not be empty!',
            ]);

        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        //$image = $request->file('image');

        //$name = 'orgd150718.png';
        if($request->file('image')!=null && $request->file('image')->isValid()) {
            try {
                $file = $request->file('image');
                $tempName = strtolower(str_replace(' ', '', $request->input('title')));
                $name = $tempName.date("dmy").'.' . $file->getClientOriginalExtension();

                $request->file('image')->move("images/post", $name);
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
//                    return false;
            }
        }
        $postImage = '';
        if(!empty($name)){
            $postImage = "images/post/".$name;
        }else{
            $postImage = $request->input('editImage');
        }

        $post = Post::findOrFail($id);

        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->image = $postImage;
        $post->description = $request->description;
        $post->status = $request->status;

        $post->save();

        return redirect()->back()->with('success','Post created successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::findOrFail($id)->delete();

        return redirect()->back();
    }
}
