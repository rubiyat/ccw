<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $viewPage = 'admin.profile.';

    public function edit($id)
    {
        $profile = User::findOrFail(Auth::user()->id);
        return view($this->viewPage.'edit',compact('profile'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ],
            [
                'email.required' => 'Email must not be empty!',
            ]);

        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        //$image = $request->file('image');

        //$name = 'orgd150718.png';
        if($request->file('image')!=null && $request->file('image')->isValid()) {
            try {
                $file = $request->file('image');
                $tempName = strtolower(str_replace(' ', '', $request->input('title')));
                $name = $tempName.date("dmy").'.' . $file->getClientOriginalExtension();

                $request->file('image')->move("images/user", $name);
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
//                    return false;
            }
        }

        if(!empty($name)){
            $profileImage = "images/user/".$name;
        }else{
            $profileImage = $request->input('editImage');
        }



        $user = User::findOrFail(Auth::user()->id);

        if($request->password){
            $password = $request->password;
        }else{
            $password = $user->password;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $password;
        $user->image = $profileImage;
        $user->bio = $request->bio;

        $user->save();

        return redirect()->back()->with('success','User update successfully!');
    }
}
