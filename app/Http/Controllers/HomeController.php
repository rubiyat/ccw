<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        $posts = Post::all();
        $bio = User::findOrFail(1);
        return view('welcome',compact('categories','posts','bio'));
    }

    public function categoryWisePost($id){
        $categories = Category::all();
        $categoryWisePost = Post::where('category_id',$id)->get();
        return view('post',compact('categoryWisePost','categories'));
    }
}
