<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'HomeController@index')->name('home');
Route::get('post/{id}','HomeController@categoryWisePost')->name('post');

Auth::routes();
Route::group(['middleware'=>'auth'], function(){
    Route::get('/dashboard','Admin\DashboardController@index')->name('dashboard');
    Route::resource('/category','Admin\CategoryController');
    Route::resource('/post','Admin\PostController');
    Route::resource('/profile','Admin\UserController');

});
