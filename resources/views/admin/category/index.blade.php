@extends('layout.backend.app')

@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary float-left">Category</h4>
            <a href="{{ route('category.create') }}" class="btn btn-primary float-right"><i class="fa fa-plus-circle" aria-hidden="true"> </i> Add New</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th style="width: 30px;">SL</th>
                        <th>Name</th>
                        <th style="width: 120px;">Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @php
                    $i = 0;
                    @endphp
                    @if(!$categories->isEmpty())
                        @foreach($categories as $row)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $row->name }}</td>
                                <td>
                                    <a href="{{ route('category.edit',$row->id) }}" class="btn btn-primary btn-circle"><i class="fas fa-edit"></i></a>

                                    <form method="POST" action="{{ route('category.destroy', [$row->id]) }}" style="display:inline-block">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-circle" type="submit"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3" style="text-align: center;">There is no data found!</td>
                        </tr>


                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection