@extends('layout.backend.app')

@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary float-left">Post</h4>
            <a href="{{ route('post.create') }}" class="btn btn-primary float-right"><i class="fa fa-plus-circle" aria-hidden="true"> </i> Add New</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th style="width: 30px;">SL</th>
                        <th style="width: 250px;">Category Name</th>
                        <th style="width: 100px;">Image</th>
                        <th>Title</th>
                        <th style="width: 40px;">Status</th>
                        <th style="width: 180px;">Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Category Name</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @php
                        $i = 0;
                    @endphp
                    @if(!$posts->isEmpty())
                        @foreach($posts as $row)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $row->category->name }}</td>
                                <td><img src="{{ $row->image }}" width="40" height="30"></td>
                                <td>{{ $row->title }}</td>
                                <td>
                                    @if($row->status == 0)
                                    <div class="p-1 bg-success text-white">Active</div>
                                    @else
                                    <div class="p-1 bg-danger text-white">Inactive</div>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('post.show',$row->id) }}" class="btn btn-primary btn-circle"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('post.edit',$row->id) }}" class="btn btn-primary btn-circle"><i class="fas fa-edit"></i></a>
                                    <form method="POST" action="{{ route('post.destroy', [$row->id]) }}" style="display:inline-block">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-circle" type="submit"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" style="text-align: center;">There is no data found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection