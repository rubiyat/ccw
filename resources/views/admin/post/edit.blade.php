@extends('layout.backend.app')
<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }
</style>
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary float-left">Edit Post</h4>
            <a href="{{ route('post.index') }}" class="btn btn-danger float-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Go Back</a>
        </div>
        <div class="card-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <form action="{{ route('post.update',$post->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="category">Category Name</label>
                    <select class="form-control" id="category" name="category_id">
                        <option>Select One</option>
                        @foreach($categories as $row)
                            <option value="{{ $row->id }}" @if($post->category_id == $row->id) selected @endif>{{ $row->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" placeholder="Example: Post One Title Goes Here" name="title" value="{{ $post->title }}">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="hidden" class="form-control-file" id="image" name="editImage" value="{{ $post->image }}">
                    <input type="file" class="form-control-file" id="image" name="image" onchange="readURL(this);"><br>
                    <img id="blah" src="{{ asset($post->image) }}" alt="your image" width="120" height="150"/>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control ck-editor__editable_inline" name="description" id="editor">{!! $post->description !!}</textarea>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" id="status" name="status">
                        <option value="0" @if($post->status == 0) selected @endif>Active</option>
                        <option value="1" @if($post->status == 1) selected @endif>Inactive</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(120)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endsection