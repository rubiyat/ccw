@extends('layout.backend.app')

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold text-primary float-left">Show Post</h4>

                </div>
                <div class="card-body">
                    <span><b>Category Name: </b></span>
                    <span>{{ $post->category->name }}</span><br><br>
                    <span><b>Post Title: </b></span>
                    <span>{{ $post->title }}</span><br><br>
                    <span><b>Description: </b></span>
                    <span>{!! $post->description !!}</span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold text-primary float-left">Show Post Image</h4>
                </div>
                <div class="card-body">
                    <img src="{{ asset($post->image) }}" width="200" height="200">
                </div>
            </div>
        </div>
    </div>





@endsection