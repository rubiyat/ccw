@extends('layout.frontend.app')

@section('content')
    @if(!$categoryWisePost->isEmpty())
        @foreach($categoryWisePost as $row)
            <div class="card mb-4">
                <img class="card-img-top" src="{{ asset($row->image) }}" alt="{{ $row->title }}">
                <div class="card-body">
                    <h2 class="card-title">{{ $row->title }}</h2>
                    <p class="card-text">{!! $row->description !!}</p>
                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                </div>
                <div class="card-footer text-muted">
                    {{ $row->created_at->diffForhumans() }}
                </div>
            </div>
        @endforeach
    @else
        <h3>No data found!</h3>
    @endif



    <!-- Pagination -->
    <ul class="pagination justify-content-center mb-4">
        <li class="page-item">
            <a class="page-link" href="#">&larr; Older</a>
        </li>
        <li class="page-item disabled">
            <a class="page-link" href="#">Newer &rarr;</a>
        </li>
    </ul>
@endsection