<div class="card my-4">
    <h5 class="card-header">Categories</h5>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled mb-0">
                    @if(!$categories->isEmpty())
                        @foreach($categories as $row)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('post',$row->id) }}">{{ $row->name }}</a>
                            </li>
                        @endforeach
                    @else
                        <li style="color: #fff;">There is no categories found</li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</div>