<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">{{ env('APP_NAME') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @if(!$categories->isEmpty())
                @foreach($categories as $row)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('post',$row->id) }}">{{ $row->name }}</a>
                    </li>
                @endforeach
                @else
                    <li style="color: #fff;">There is no menu found</li>
                @endif
            </ul>
        </div>
    </div>
</nav>