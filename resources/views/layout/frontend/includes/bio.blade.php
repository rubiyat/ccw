<style>
    .dot {
        height: 180px;
        width: 180px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
    }
</style>

<div class="card my-4">
    <h5 class="card-header">About Me</h5>
    <div class="card-body" style="text-align: center;">
            <img class="dot" src="{{ asset($bio->image) }}">
        <br>
        <span>Hello, I am </span><b>{{ $bio->name }}</b>
       {!! $bio->bio !!}
    </div>
</div>